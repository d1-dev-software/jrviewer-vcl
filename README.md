# JrViewer
`JrViewer` is a simple client application for checking the `jrbustcp` connection and viewing data.

## References
[JrBusTcp Protocol Description](http://prom-auto.ru/wiki/doku.php?id=doc:jroboplc:modules:jrbustcp-protocol)

## Change Log

### [1.0.0]
Initial version