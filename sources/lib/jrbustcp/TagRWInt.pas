unit TagRWInt;

interface
uses
  System.SysUtils,
  Tags;

type
  TTagRWInt = class(TTagRW)
  private
	  FValueRd: Integer;
	  FValueWr: Integer;
    FValueWrLast: Integer;
  protected
	  procedure copyLastWriteToRead; override;   // ???
	  procedure copyWriteToLastWrite; override;
	  procedure copyWriteToRead; override;
  public
    constructor Create(const name: string; flags: Integer = 0);override;

    function getType: TTagType; override;
    function equalsValue(tag: TTag): Boolean; override;

    function getBool: boolean; override;
    function getInt: integer; override;
    function getLong: int64; override;
    function getDouble: double; override;
    function getString: string; override;

    procedure setBool(const value: boolean); override;
    procedure setInt(const value: integer); override;
    procedure setLong(const value: int64); override;
    procedure setDouble(const value: double); override;
    procedure setString(const value: string); override;


    function getWriteValBool: boolean; override;
    function getWriteValInt: integer; override;
    function getWriteValLong: int64; override;
    function getWriteValDouble: double; override;
    function getWriteValString: string; override;

    procedure setReadValBool(const value: boolean); override;
    procedure setReadValInt(const value: integer); override;
    procedure setReadValLong(const value: int64); override;
    procedure setReadValDouble(const value: double); override;
    procedure setReadValString(const value: string); override;
  end;


implementation

{ TTagRWInt }

constructor TTagRWInt.Create(const name: string; flags: Integer);
begin
  inherited Create(name, flags);
  FValueRd := 0;
  FValueWr := 0;
  FValueWrLast := 0;
end;


function TTagRWInt.getType: TTagType;
begin
  Result := ttINT;
end;

function TTagRWInt.equalsValue(tag: TTag): Boolean;
begin
  Result := tag.getInt() = FValueRd;
end;

procedure TTagRWInt.copyWriteToLastWrite;
begin
	FValueWrChanged := false;
	FValueWrLast := FValueWr;
end;

procedure TTagRWInt.copyLastWriteToRead;
begin
	FValueRd := FValueWrLast;
end;

procedure TTagRWInt.copyWriteToRead;
begin
	FValueRd := FValueWr;
end;


// general getters
function TTagRWInt.getBool: boolean;
begin
  Result := FValueRd <> 0;
end;

function TTagRWInt.getInt: integer;
begin
  Result := FValueRd;
end;

function TTagRWInt.getLong: int64;
begin
  Result := FValueRd;
end;

function TTagRWInt.getDouble: double;
begin
  Result := FValueRd;
end;

function TTagRWInt.getString: string;
begin
  Result := IntToStr(FValueRd);
end;


// general setters
procedure TTagRWInt.setBool(const value: boolean);
begin
  System.TMonitor.Enter(self);
  if value then
    FValueWr := 1
  else
    FValueWr := 0;
	FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWInt.setInt(const value: integer);
begin
  System.TMonitor.Enter(self);
  FValueWr := value;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWInt.setLong(const value: int64);
begin
  System.TMonitor.Enter(self);
  FValueWr := value;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWInt.setDouble(const value: double);
begin
  System.TMonitor.Enter(self);
  FValueWr := Trunc(value);
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWInt.setString(const value: string);
begin
  System.TMonitor.Enter(self);
  FValueWr := StrToIntDef(value, 0);
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;




// rw getters
function TTagRWInt.getWriteValBool: boolean;
begin
  Result := FValueWrLast <> 0;
end;

function TTagRWInt.getWriteValInt: integer;
begin
  Result := FValueWrLast;
end;

function TTagRWInt.getWriteValLong: int64;
begin
  Result := FValueWrLast;
end;

function TTagRWInt.getWriteValDouble: double;
begin
  Result := FValueWrLast;
end;

function TTagRWInt.getWriteValString: string;
begin
  Result := IntToStr(FValueWrLast);
end;


// rw getters
procedure TTagRWInt.setReadValBool(const value: boolean);
begin
  if value then
    FValueRd := 1
  else
    FValueRd := 0;
end;

procedure TTagRWInt.setReadValInt(const value: integer);
begin
  FValueRd := value;
end;

procedure TTagRWInt.setReadValLong(const value: int64);
begin
  FValueRd := value;
end;

procedure TTagRWInt.setReadValDouble(const value: double);
begin
  FValueRd := Trunc(value);
end;

procedure TTagRWInt.setReadValString(const value: string);
begin
	FValueRd := StrToIntDef(value, 0);
end;

end.
