unit TagRWBool;

interface
uses
  Tags;

type
  TTagRWBool = class(TTagRW)
  private
	  FValueRd: Boolean;
	  FValueWr: Boolean;
    FValueWrLast: Boolean;
  protected
	  procedure copyLastWriteToRead; override;   // ???
	  procedure copyWriteToLastWrite; override;
	  procedure copyWriteToRead; override;
  public
    constructor Create(const name: string; flags: Integer = 0);override;

    function getType: TTagType; override;
    function equalsValue(tag: TTag): Boolean; override;

    function getBool: boolean; override;
    function getInt: integer; override;
    function getLong: int64; override;
    function getDouble: double; override;
    function getString: string; override;

    procedure setBool(const value: boolean); override;
    procedure setInt(const value: integer); override;
    procedure setLong(const value: int64); override;
    procedure setDouble(const value: double); override;
    procedure setString(const value: string); override;


    function getWriteValBool: boolean; override;
    function getWriteValInt: integer; override;
    function getWriteValLong: int64; override;
    function getWriteValDouble: double; override;
    function getWriteValString: string; override;

    procedure setReadValBool(const value: boolean); override;
    procedure setReadValInt(const value: integer); override;
    procedure setReadValLong(const value: int64); override;
    procedure setReadValDouble(const value: double); override;
    procedure setReadValString(const value: string); override;
  end;


implementation

{ TTagRWBool }

constructor TTagRWBool.Create(const name: string; flags: Integer);
begin
  inherited Create(name, flags);
  FValueRd := false;
  FValueWr := false;
  FValueWrLast := false;
end;


function TTagRWBool.getType: TTagType;
begin
  Result := ttBOOL;
end;

function TTagRWBool.equalsValue(tag: TTag): Boolean;
begin
  Result := tag.getBool() = FValueRd;
end;

procedure TTagRWBool.copyLastWriteToRead;
begin
		FValueRd := FValueWrLast;
end;

procedure TTagRWBool.copyWriteToLastWrite;
begin
	FValueWrChanged := false;
	FValueWrLast := FValueWr;
end;

procedure TTagRWBool.copyWriteToRead;
begin
	FValueRd := FValueWr;
end;


// general getters
function TTagRWBool.getBool: boolean;
begin
  Result := FValueRd;
end;

function TTagRWBool.getInt: integer;
begin
  if FValueRd then
    Result := 1
  else
    Result := 0;
end;

function TTagRWBool.getLong: int64;
begin
  if FValueRd then
    Result:=1
  else
    Result:=0;
end;

function TTagRWBool.getDouble: double;
begin
  if FValueRd then
    Result := 1.0
  else
    Result := 0.0;
end;

function TTagRWBool.getString: string;
begin
  if FValueRd then
    Result := BOOL_VALUE_TRUE
  else
    Result := BOOL_VALUE_FALSE;
end;


// general setters
procedure TTagRWBool.setBool(const value: boolean);
begin
  System.TMonitor.Enter(self);
  FValueWr := value;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWBool.setInt(const value: integer);
begin
  System.TMonitor.Enter(self);
  FValueWr := value <> 0;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWBool.setLong(const value: int64);
begin
  System.TMonitor.Enter(self);
  FValueWr := value <> 0;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWBool.setDouble(const value: double);
begin
  System.TMonitor.Enter(self);
  FValueWr := value <> 0.0;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWBool.setString(const value: string);
begin
  System.TMonitor.Enter(self);
  FValueWr := value = BOOL_VALUE_TRUE;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;




// rw getters
function TTagRWBool.getWriteValBool: boolean;
begin
  Result := FValueWrLast;
end;

function TTagRWBool.getWriteValInt: integer;
begin
  if FValueWrLast then Result:=1 else Result:=0;
end;

function TTagRWBool.getWriteValLong: int64;
begin
  if FValueWrLast then Result:=1 else Result:=0;
end;

function TTagRWBool.getWriteValDouble: double;
begin
  if FValueWrLast then Result:=1.0 else Result:=0.0;
end;

function TTagRWBool.getWriteValString: string;
begin
  if FValueWrLast then
    Result := BOOL_VALUE_TRUE
  else
    Result := BOOL_VALUE_FALSE;
end;


// rw getters
procedure TTagRWBool.setReadValBool(const value: boolean);
begin
  FValueRd := value;
end;

procedure TTagRWBool.setReadValInt(const value: integer);
begin
  FValueRd := value <> 0;
end;

procedure TTagRWBool.setReadValLong(const value: int64);
begin
  FValueRd := value <> 0;
end;

procedure TTagRWBool.setReadValDouble(const value: double);
begin
  FValueRd := value <> 0.0;
end;

procedure TTagRWBool.setReadValString(const value: string);
begin
	FValueRd := value = BOOL_VALUE_TRUE;
end;



end.
