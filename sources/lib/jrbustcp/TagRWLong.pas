unit TagRWLong;

interface
uses
  System.SyncObjs,
  System.SysUtils,
  Tags;

type
  TTagRWLong = class(TTagRW)
  private
    FLockRd: TLightweightMREW;
    FLockWrLast: TLightweightMREW;

	  FValueRd: Int64;
	  FValueWr: Int64;
    FValueWrLast: Int64;
  protected
	  procedure copyLastWriteToRead; override;   // ???
	  procedure copyWriteToLastWrite; override;
	  procedure copyWriteToRead; override;
  public
    constructor Create(const name: string; flags: Integer = 0);override;

    function getType: TTagType; override;
    function equalsValue(tag: TTag): Boolean; override;

    function getBool: boolean; override;
    function getInt: integer; override;
    function getLong: int64; override;
    function getDouble: double; override;
    function getString: string; override;

    procedure setBool(const value: boolean); override;
    procedure setInt(const value: integer); override;
    procedure setLong(const value: int64); override;
    procedure setDouble(const value: double); override;
    procedure setString(const value: string); override;


    function getWriteValBool: boolean; override;
    function getWriteValInt: integer; override;
    function getWriteValLong: int64; override;
    function getWriteValDouble: double; override;
    function getWriteValString: string; override;

    procedure setReadValBool(const value: boolean); override;
    procedure setReadValInt(const value: integer); override;
    procedure setReadValLong(const value: int64); override;
    procedure setReadValDouble(const value: double); override;
    procedure setReadValString(const value: string); override;
  end;


implementation

{ TTagRWLong }

constructor TTagRWLong.Create(const name: string; flags: Integer);
begin
  inherited Create(name, flags);
  FValueRd := 0;
  FValueWr := 0;
  FValueWrLast := 0;
end;


function TTagRWLong.getType: TTagType;
begin
  Result := ttLONG;
end;

function TTagRWLong.equalsValue(tag: TTag): Boolean;
begin
  FLockRd.BeginRead;
  Result := tag.getLong() = FValueRd;
  FLockRd.EndRead;
end;

procedure TTagRWLong.copyLastWriteToRead;
begin
  FLockRd.BeginWrite;
  FValueRd := FValueWrLast;
  FLockRd.EndWrite;
end;

procedure TTagRWLong.copyWriteToLastWrite;
begin
  FLockWrLast.BeginWrite;
  FValueWrChanged := false;
	FValueWrLast := FValueWr;
  FLockWrLast.EndWrite;
end;

procedure TTagRWLong.copyWriteToRead;
begin
  FLockRd.BeginWrite;
  FValueRd := FValueWr;
  FLockRd.EndWrite;
end;


// general getters
function TTagRWLong.getBool: boolean;
begin
  FLockRd.BeginRead;
  Result := FValueRd <> 0;
  FLockRd.EndRead;
end;

function TTagRWLong.getInt: integer;
begin
  FLockRd.BeginRead;
  Result := FValueRd and $FFFFFFFF;
  FLockRd.EndRead;
end;

function TTagRWLong.getLong: int64;
begin
  FLockRd.BeginRead;
  Result := FValueRd;
  FLockRd.EndRead;
end;

function TTagRWLong.getDouble: double;
begin
  FLockRd.BeginRead;
  Result := FValueRd;
  FLockRd.EndRead;
end;

function TTagRWLong.getString: string;
begin
  FLockRd.BeginRead;
  Result := IntToStr(FValueRd);
  FLockRd.EndRead;
end;


// general setters
procedure TTagRWLong.setBool(const value: boolean);
begin
  System.TMonitor.Enter(self);
  if value then
    FValueWr := 1
  else
    FValueWr := 0;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWLong.setInt(const value: integer);
begin
  System.TMonitor.Enter(self);
  FValueWr := value;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWLong.setLong(const value: int64);
begin
  System.TMonitor.Enter(self);
  FValueWr := value;
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWLong.setDouble(const value: double);
begin
  System.TMonitor.Enter(self);
  FValueWr := Trunc(value);
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;

procedure TTagRWLong.setString(const value: string);
begin
  System.TMonitor.Enter(self);
  FValueWr := StrToInt64Def(value, 0);
  FValueWrChanged := true;
  System.TMonitor.Exit(self);
end;




// rw getters
function TTagRWLong.getWriteValBool: boolean;
begin
  FLockWrLast.BeginRead;
  Result := FValueWrLast <> 0;
  FLockWrLast.EndRead;
end;

function TTagRWLong.getWriteValInt: integer;
begin
  FLockWrLast.BeginRead;
  Result := FValueWrLast and $FFFFFFFF;
  FLockWrLast.EndRead;
end;

function TTagRWLong.getWriteValLong: int64;
begin
  FLockWrLast.BeginRead;
  Result := FValueWrLast;
  FLockWrLast.EndRead;
end;

function TTagRWLong.getWriteValDouble: double;
begin
  FLockWrLast.BeginRead;
  Result := FValueWrLast;
  FLockWrLast.EndRead;
end;

function TTagRWLong.getWriteValString: string;
begin
  FLockWrLast.BeginRead;
  Result := IntToStr(FValueWrLast);
  FLockWrLast.EndRead;
end;


// rw getters
procedure TTagRWLong.setReadValBool(const value: boolean);
begin
  FLockRd.BeginWrite;
  if value then
    FValueRd := 1
  else
    FValueRd := 0;
  FLockRd.EndWrite;
end;

procedure TTagRWLong.setReadValInt(const value: integer);
begin
  FLockRd.BeginWrite;
  FValueRd := value;
  FLockRd.EndWrite;
end;

procedure TTagRWLong.setReadValLong(const value: int64);
begin
  FLockRd.BeginWrite;
  FValueRd := value;
  FLockRd.EndWrite;
end;

procedure TTagRWLong.setReadValDouble(const value: double);
begin
  FLockRd.BeginWrite;
  FValueRd := Trunc(value);
  FLockRd.EndWrite;
end;

procedure TTagRWLong.setReadValString(const value: string);
begin
  FLockRd.BeginWrite;
 	FValueRd := StrToInt64Def(value, 0);
  FLockRd.EndWrite;
end;

end.
