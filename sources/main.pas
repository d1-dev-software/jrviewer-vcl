unit main;
interface
uses
  Tags,
  JrbustcpClient,

  Generics.Collections,
  Generics.Defaults,
  RegularExpressions,
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  IdTCPClient,
  IdGlobal,
  ExtCtrls,
  ComCtrls,
  Grids,
  Clipbrd,
  Menus,
  Math;


type
  TForm1 = class(TForm)
    timer: TTimer;
    edPort: TEdit;
    cmbLocalFilter: TComboBox;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    edHost: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    edPeriod: TEdit;
    Label8: TLabel;
    pnlConnect: TPanel;
    edClientDescr: TEdit;
    edRemoteFilter: TEdit;
    pnlTotal: TPanel;
    Label3: TLabel;
    pnlListSize: TPanel;
    lbListSize: TLabel;
    cbExcludeExternal: TCheckBox;
    cbIncludeHidden: TCheckBox;
    cbAuth: TCheckBox;
    Label2: TLabel;
    edAuthKeyName: TEdit;
    grid: TStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure pnlConnectClick(Sender: TObject);
    procedure timerTimer(Sender: TObject);
    procedure cmbLocalFilterKeyPress(Sender: TObject; var Key: Char);
    procedure mnCopyToClipboardClick(Sender: TObject);
    procedure mnSelectAllClick(Sender: TObject);
    procedure edPeriodChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure gridGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure gridGetEditText(Sender: TObject; ACol, ARow: Integer;
      var Value: string);
    procedure gridSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure gridKeyPress(Sender: TObject; var Key: Char);
    procedure cmbLocalFilterChange(Sender: TObject);
    procedure gridFixedCellClick(Sender: TObject; ACol, ARow: Integer);
  private
    connected: boolean;
    clnt: TJrbustcpClient;
    iniFileName: string;
    listsize: integer;
    runOnStartup: boolean;
    FEditRow: integer;
    FEditOk: boolean;
    FEditReadonlyText: string;
    FFilterRegex: TRegEx;
    FFilterText: string;
    FSortColumn: integer;
    procedure UpdateConnectPanel;
    procedure UpdateList;
    procedure UpdateValues;
    procedure ApplyLocalFilter;
    procedure LoadParamStr;
    procedure LoadIni;
    procedure SaveIni;
    function CheckFilter(const src: string): boolean;
    procedure setDefaultFilterRegex;
    procedure SetSorting;
    function GetTagValueFormatted(Tag: TTag): string;
  public
  end;

var
  Form1: TForm1;
implementation
{$R *.dfm}
uses
  inifiles;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown := True;
  runOnStartup := false;
  grid.doublebuffered := true;
  grid.Cells[0,0] := 'Name ';
  grid.Cells[1,0] := 'Type ';
  grid.Cells[2,0] := 'Value ';
  grid.ColWidths[0] := 300;
  grid.ColWidths[1] := 80;
  grid.ColWidths[2] := 180;
  grid.ColAlignments[0] := taLeftJustify;
  grid.ColAlignments[1] := taCenter;
  grid.ColAlignments[2] := taRightJustify;
  iniFileName := ChangeFileExt(Application.ExeName, '.ini');
  LoadIni;
  LoadParamStr;
  clnt := TJrbustcpClient.Create;
  connected := false;
  UpdateConnectPanel;
  setDefaultFilterRegex();
  listsize:=0;
  FSortColumn := 0;
  SetSorting();
end;
procedure TForm1.SetSorting;
begin
  for var i:=0 to grid.ColCount-1 do
  begin
    var c := ' ';
    if i = FSortColumn then
      c := #8595;
    var s := grid.Cells[i, 0];
    delete(s, s.Length, 1);
    grid.Cells[i, 0] := s + c;
  end;
end;
procedure TForm1.FormDestroy(Sender: TObject);
begin
  clnt.Free;
  SaveIni;
end;

procedure TForm1.pnlConnectClick(Sender: TObject);
begin
  connected := not connected;
  pnlConnect.BevelOuter := bvLowered;
  Application.ProcessMessages;
  if connected then begin
    clnt.host := edHost.Text;
    clnt.port := StrToIntDef(edPort.Text, 0);
    clnt.filter := edRemoteFilter.Text;
    clnt.descr := edClientDescr.Text;
    clnt.period := StrToIntDef(edPeriod.Text, 1000);
    timer.Interval := clnt.period;
    clnt.authKeyName := edAuthKeyName.Text;
    clnt.setFlag(INITPRM_EXCLUDE_EXTERNAL, cbExcludeExternal.Checked);
    clnt.setFlag(INITPRM_INCLUDE_HIDDEN, cbIncludeHidden.Checked);
    clnt.auth := cbAuth.Checked;
    clnt.Connect;
    UpdateList;
  end else begin
    clnt.Disconnect;
  end;
  pnlConnect.BevelOuter := bvRaised;
  UpdateConnectPanel;
end;

procedure TForm1.UpdateConnectPanel;
const
  clOn = clWhite;
  clOff = clBtnFace;
var
  cl1, cl2: TColor;
  i: integer;
begin
  if connected then begin
    pnlConnect.Color := $004242FF;
    pnlConnect.Font.Color := clWhite;
    pnlConnect.Caption := 'Disconnect';
    cl1 := clOff;
    cl2 := clOn;
  end else begin
    pnlConnect.Color := $0080FF80;
    pnlConnect.Font.Color := clBlack;
    pnlConnect.Caption := 'Connect';
    cl1 := clOn;
    cl2 := clOff;
  end;
  edClientDescr.Color     := cl1;
  edHost.Color            := cl1;
  edPort.Color            := cl1;
  edRemoteFilter.Color    := cl1;
  edRemoteFilter.Color    := cl1;
  edAuthKeyName.Color     := cl1;
  cmbLocalFilter.Color    := cl2;
end;

procedure TForm1.timerTimer(Sender: TObject);
begin
  if connected then begin
    if not clnt.Connected then begin
      pnlConnectClick(nil);
      Exit;
    end;
    if clnt.NeedReinitList then begin
      clnt.Disconnect;
      clnt.Connect;
      UpdateList;
    end;
    UpdateValues;
  end;
end;

procedure TForm1.setDefaultFilterRegex;
begin
  FFilterRegex := TRegEx.Create('.*');
end;

function TForm1.CheckFilter(const src: string): boolean;
begin
  if FFilterText <> cmbLocalFilter.Text then
  begin
    FFilterText := cmbLocalFilter.Text;
    if trim(FFilterText) = '' then
      setDefaultFilterRegex()
    else
      try
        FFilterRegex := TRegEx.Create(FFilterText, [roCompiled, roNotEmpty]);
      except
        setDefaultFilterRegex();
      end;
  end;

  result := FFilterRegex.IsMatch(src);
end;


function TForm1.GetTagValueFormatted(Tag: TTag): string;
begin
  case Tag.getType of
    ttBOOL, ttSTRING: result := tag.getString;
    ttINT, ttLONG: result := format('%14d      ', [tag.getLong]);
    ttDOUBLE: result := format('%20.5f', [tag.getDouble]);
  end;
end;

procedure TForm1.UpdateList;
var
  i,r,tblsize: Integer;
  tag: TTag;
  itags: array of integer;
  filter: string;
begin
  tblsize := clnt.TagTable.getSize;
  SetLength(itags, tblsize);
  listsize := 0;
  for i:=0 to tblsize-1 do begin
    tag := clnt.TagTable.get(i);
    if CheckFilter(tag.getName) then begin
      itags[ listsize ] := i;
      inc(listsize);
    end;
  end;
  SetLength(itags, listsize);

  TArray.Sort<integer>(itags, TDelegatedComparer<integer>.Construct(
    function(const i1, i2: integer): Integer
    const
      fmt0 = '%s';
      fmt1 = '%s%s';
      fmt2 = '%s%s%s';
    var
      s1, s2, fmt: string;
      tag1, tag2: TTag;
    begin
      tag1 := clnt.TagTable.get(i1);
      tag2 := clnt.TagTable.get(i2);
      case FSortColumn of
        0: begin
          s1 := format(fmt0, [tag1.getName]);
          s2 := format(fmt0, [tag2.getName]);
        end;

        1: begin
          s1 := format(fmt1, [tag1.getTypeName, tag1.getName]);
          s2 := format(fmt1, [tag2.getTypeName, tag2.getName]);
        end;

        2: begin
          s1 := format(fmt2, [GetTagValueFormatted(tag1), tag1.getTypeName, tag1.getName]);
          s2 := format(fmt2, [GetTagValueFormatted(tag2), tag2.getTypeName, tag2.getName]);
        end;
      end;

      Result := CompareStr(s1, s2);
    end));

  pnlTotal.Caption := IntToStr(clnt.TagTable.getSize);
  pnlListSize.Caption := IntToStr(listsize);

  if listsize = 0 then begin
    grid.RowCount := 2;
    grid.Cells[0,1] := '';
    grid.Cells[1,1] := '';
    grid.Cells[2,1] := '';
  end else
    grid.RowCount := listsize+1;
  for i:=0 to listsize-1 do begin
    tag := clnt.TagTable.get(itags[i]);
    r := i+1;
    if grid.Cells[0,r] <> tag.getName then
      grid.Cells[0,r] := tag.getName;
    // todo: descr
    if grid.Cells[1,r] <> tag.getTypeName then
      grid.Cells[1,r] := tag.getTypeName;
    if grid.Cells[2,r] <> tag.getString then
      grid.Cells[2,r] := tag.getString;
    grid.Objects[0,r] := Pointer(itags[i]);
  end;
end;

procedure TForm1.UpdateValues;
var
  i,n,index: Integer;
  tag: TTag;
  cl: TColor;
begin
  n := min(grid.RowCount, grid.TopRow + grid.VisibleRowCount);
  for i:=grid.TopRow to n do begin
    index := Integer(grid.Objects[0,i]);
    if index >= clnt.TagTable.getSize then begin
      Continue;
    end;
    tag := clnt.TagTable.get( index );
    if (grid.EditorMode) and (i = FEditRow) then
      Continue;
    if grid.Cells[2,i] <> tag.getString then begin
      grid.Cells[2,i] := tag.getString;
    end;
  end;
end;

procedure TForm1.ApplyLocalFilter;
begin
  if (cmbLocalFilter.Items.count = 0) or (cmbLocalFilter.Items[0] <> cmbLocalFilter.Text) then
    cmbLocalFilter.Items.Insert(0, cmbLocalFilter.Text );
  UpdateList;
end;

procedure TForm1.cmbLocalFilterChange(Sender: TObject);
begin
  ApplyLocalFilter;
end;

procedure TForm1.cmbLocalFilterKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    ApplyLocalFilter;
    Key := #0;
  end;
end;

procedure TForm1.LoadParamStr;
var
  i,k: Integer;
  prm,value: string;
begin
    for i:=1 to ParamCount do begin
      prm := trim(ParamStr(i));
      k := Pos('=', prm);
      if k = -1 then Continue;
      value := Copy(prm, k+1, Length(prm)-k);
      Delete(prm, k, Length(prm)-k+1);
      prm := UpperCase(prm);
      if prm = '-FILTER' then edRemoteFilter.Text := value;
      if prm = '-DESCR' then edClientDescr.Text := value;
      if prm = '-HOST' then edHost.Text := value;
      if prm = '-PORT' then edPort.Text := value;
      if prm = '-PERIOD' then edPeriod.Text := value;
      if prm = '-LOCALFILTER' then cmbLocalFilter.Text := value;
      if prm = '-START' then runOnStartup := true;
    end;
end;

procedure TForm1.LoadIni;
var
  gr: string;
  flags: integer;
begin
  with TIniFile.Create(iniFileName) do try
    gr := 'JrbustcpClient';;
    edRemoteFilter.Text := ReadString(gr, 'filter', '');
    edClientDescr.Text  := ReadString(gr, 'descr', 'viewer');
    edHost.Text         := ReadString(gr, 'host', 'localhost');
    edPort.Text         := ReadString(gr, 'port', '30000');
    edPeriod.Text       := ReadString(gr, 'period', '500');
    cmbLocalFilter.Text := ReadString(gr, 'localfilter', '*');
    cbExcludeExternal.Checked := ReadBool(gr, 'excludeExternal', false);
    cbIncludeHidden.Checked := ReadBool(gr, 'includeHidden', false);
    cbAuth.Checked := ReadBool(gr, 'auth', false);
    edAuthKeyName.Text  := ReadString(gr, 'authKeyName', '');

    gr := 'MainForm';
    Form1.Left   := ReadInteger(gr, 'Left', Form1.Left);
    Form1.Top    := ReadInteger(gr, 'Top', Form1.Top);
    Form1.Width  := ReadInteger(gr, 'Width', Form1.Width);
    Form1.Height := ReadInteger(gr, 'Height', Form1.Height);
    grid.ColWidths[0] := ReadInteger(gr, 'ColWidth0', grid.ColWidths[0]);
    grid.ColWidths[1] := ReadInteger(gr, 'ColWidth1', grid.ColWidths[1]);
    grid.ColWidths[2] := ReadInteger(gr, 'ColWidth2', grid.ColWidths[2]);
  finally
    Free;
  end;
end;

procedure TForm1.SaveIni;
var
  gr: string;
begin
  with TIniFile.Create(iniFileName) do try
    gr := 'JrbustcpClient';;
    writeString(gr, 'filter', edRemoteFilter.Text);
    writeString(gr, 'descr', edClientDescr.Text);
    writeString(gr, 'host', edHost.Text);
    writeString(gr, 'port', edPort.Text);
    writeString(gr, 'period', edPeriod.Text);
    writeString(gr, 'localfilter', cmbLocalFilter.Text);
    WriteBool(gr, 'excludeExternal', cbExcludeExternal.Checked);
    WriteBool(gr, 'includeHidden', cbIncludeHidden.Checked);
    WriteBool(gr, 'auth', cbAuth.Checked);
    writeString(gr, 'authKeyName', edAuthKeyName.Text);
    gr := 'MainForm';
    writeInteger(gr, 'Left', Form1.Left);
    writeInteger(gr, 'Top', Form1.Top);
    writeInteger(gr, 'Width', Form1.Width);
    writeInteger(gr, 'Height', Form1.Height);
    writeInteger(gr, 'ColWidth0', grid.ColWidths[0]);
    writeInteger(gr, 'ColWidth1', grid.ColWidths[1]);
    writeInteger(gr, 'ColWidth2', grid.ColWidths[2]);
  finally
    Free;
  end;
end;


procedure TForm1.mnCopyToClipboardClick(Sender: TObject);
var
  Clip: TClipboard;
begin
    Clip := TClipboard.Create;
//    Clip.AsText := grid.SelectedText;
    Clip.Free;
end;

procedure TForm1.mnSelectAllClick(Sender: TObject);
begin
//  grid.SelectRange(0,grid.ColCount-1, 1,grid.RowCount-1);
end;

procedure TForm1.edPeriodChange(Sender: TObject);
var
  period: Integer;
begin
  if not connected then Exit;
  
  period := StrToIntDef(edPeriod.Text, 1000);
  if period < 10 then begin
    period := 10;
    edPeriod.Text := '10';
  end;
  clnt.period := period;
  timer.Interval := period;
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
  if runOnStartup then
    pnlConnectClick(nil);
end;

procedure TForm1.gridGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
var
  index: Integer;
  tag: TTag;
  cl: TColor;
begin
  cl := clBlack;
  if ARow > 0 then begin
    index := Integer(grid.Objects[0,ARow]);
    if index < clnt.TagTable.getSize then begin
      tag := clnt.TagTable.get( index );
      if tag.getStatus <> tsGOOD then
        cl := clRed;
    end;
  end;
  AFont.Color := cl;
end;

procedure TForm1.gridGetEditText(Sender: TObject; ACol, ARow: Integer;
  var Value: string);
begin
  if listsize < 2 then
  begin
    grid.EditorMode := false;
    exit;
  end;

  if ACol = 2 then
  begin
    FEditRow := ARow;
    FEditOk := false;
  end
  else
    FEditReadonlyText := grid.Cells[ACol, ARow];
end;


procedure TForm1.gridKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and grid.EditorMode then
    FEditOk := true;
end;

procedure TForm1.gridSetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: string);
begin
  if ACol = 2 then
  begin
    if FEditOk and (listsize > 1) then
      clnt.TagTable.get( Integer(grid.Objects[0, ARow]) ).setString( grid.Cells[acol, arow] );
  end
  else
    grid.Cells[ACol, ARow] := FEditReadonlyText;
end;


procedure TForm1.gridFixedCellClick(Sender: TObject; ACol, ARow: Integer);
var
  LCurrent: pointer;
begin
  LCurrent := grid.Objects[0, grid.Row];
  FSortColumn := ACol;
  SetSorting;
  UpdateList;

  for var i:=1 to grid.RowCount do
    if grid.Objects[0, i] = LCurrent then
    begin
      grid.Row := i;
      break;
    end;
end;


end.
